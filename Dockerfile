FROM jboss/keycloak

ADD assets/truststore.jks /opt/jboss/keycloak/standalone/configuration/keystores/
ENV TRUSTSTORE_PWD="put-your-truststore-password"
ENV KEYCLOAK_USER='admin'
ENV KEYCLOAK_PASSWORD='admin'
ENV JAVA_OPTS='"-Djavax.net.ssl.trustStore=/opt/jboss/keycloak/standalone/configuration/keystores/truststore.jks" "-Djavax.net.ssl.trustStorePassword=${TRUSTSTORE_PWD}" "-Djava.net.preferIPv4Stack=true"'